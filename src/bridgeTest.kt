fun main() {
    val motorDiesel = Diesel()
    val berlina = Berlina(motorDiesel, 4)
    berlina.MostrarCaracteristicas()
    berlina.Acelerar(2.4)
}

interface IMotor {
    fun InyectarCombustible(cantidad: Double)
    fun ConsumirCombustible()
}

class Diesel : IMotor {
    override fun InyectarCombustible(cantidad: Double)
    {
        println("Inyectando " + cantidad + " ml. de Gasolina")
    }

    override fun ConsumirCombustible() {
        RealizarExplosion()
    }

    fun RealizarExplosion() {
        println("Realizada la explosión del Gasolina");
    }
}

abstract class Vehiculo(private val motor: IMotor) {
    fun Acelerar(combustible: Double) {
        motor.InyectarCombustible(combustible)
        motor.ConsumirCombustible()
    }
    fun Frenar() {
        println("El vehículo está frenando.")
    }

    abstract fun MostrarCaracteristicas()
}

class Berlina(motor: IMotor) : Vehiculo(motor) {
    private var capacidadMaletero: Int = 0

    constructor(motor: IMotor, capacidadMaletero: Int) : this(motor) {
        this.capacidadMaletero = capacidadMaletero;
    }

    override fun MostrarCaracteristicas()
    {
        println("Vehiculo de tipo Berlina con un maletero con una capacidad de " + capacidadMaletero + " litros.");
    }
}
